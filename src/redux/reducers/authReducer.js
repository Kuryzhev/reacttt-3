import { authConstants } from "../constants";
import { getUser } from "../../helpers/sessionStorageHelpers";

const initialState = getUser ?
    {username: getUser, isAuthenticated: true}
    : {username: "", isAuthenticated: false};

export default (state = initialState, action) => {
    switch (action.type) {
        case authConstants.LOGIN_SUCCESS:
            return {...state, username: action.payload, isAuthenticated: true};
        case authConstants.LOGOUT_SUCCESS:
            return {isAuthenticated: false};
        default:
            return {...state};
    }
};
