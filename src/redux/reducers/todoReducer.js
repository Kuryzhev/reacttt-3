import { todoConstants } from "../constants";

const initialState = {
    todoList: [],
    todoDetails: null,
    todoError: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case todoConstants.GET_TODO_LIST_SUCCESS:
            return {...state, todoList: action.payload};
        case todoConstants.GET_TODO_SUCCESS:
            return {...state, todoDetails: action.payload, todoError: null};
        case todoConstants.GET_TODO_FAILURE:
            return {...state, todoError: "Todo not found!"};
        case todoConstants.REMOVE_TODO:
            return {...state, todoDetails: null, todoError: null};
        default:
            return {...state};
    }
};
