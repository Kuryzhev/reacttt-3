import { todoConstants } from "../constants";

export const todoActions = {
    get_todoList_request,
    get_todoList_success,
    get_todoList_failure,

    get_todo_request,
    get_todo_success,
    get_todo_failure,

    remove_todo
};

function get_todoList_request() {
    return {
        type: todoConstants.GET_TODO_LIST_REQUEST,
    };
}

function get_todoList_success(todoList) {
    return {
        type: todoConstants.GET_TODO_LIST_SUCCESS,
        payload: todoList
    };
}

function get_todoList_failure(e) {
    return {
        type: todoConstants.GET_TODO_LIST_FAILURE,
        payload: e
    };
}

function get_todo_request(id) {
    return {
        type: todoConstants.GET_TODO_REQUEST,
        payload: id
    };
}

function get_todo_success(todo) {
    return {
        type: todoConstants.GET_TODO_SUCCESS,
        payload: todo
    };
}

function get_todo_failure(e) {
    return {
        type: todoConstants.GET_TODO_FAILURE,
        payload: e
    };
}

function remove_todo() {
    return {
        type: todoConstants.REMOVE_TODO
    };
}
