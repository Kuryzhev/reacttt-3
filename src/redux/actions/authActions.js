import {authConstants} from "../constants";

export const authActions = {
    //--log-in--//
    login_request,
    login_success,
    login_failure,

    //--log-out--//
    logout_request,
    logout_success,
    logout_failure,
};

function login_request(username) {
    return {
        type: authConstants.LOGIN_REQUEST,
        payload: username
    };
}

function login_success(username) {
    return {
        type: authConstants.LOGIN_SUCCESS,
        payload: username
    };
}

function login_failure(e) {
    return {
        type: authConstants.LOGIN_FAILURE,
        payload: e
    };
}

function logout_request() {
    return {
        type: authConstants.LOGOUT_REQUEST
    };
}

function logout_success() {
    return {
        type: authConstants.LOGOUT_SUCCESS
    };
}

function logout_failure() {
    return {
        type: authConstants.LOGOUT_FAILURE
    };
}
