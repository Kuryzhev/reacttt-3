import React from 'react';
import { Provider } from "react-redux";

import { AppRoute } from "./routes/AppRoute";
import store from "./redux/store";

const App = () => {
  return (
      <Provider store={store}>
        <AppRoute/>
      </Provider>
  );
};
export default App;
