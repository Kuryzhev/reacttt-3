import { takeEvery } from "redux-saga/effects";

import { authConstants, todoConstants } from "../redux/constants";
import { logOut, signIn } from "./authSaga";
import { getTodo, getTodoList } from "./todoSaga";

export function* rootSaga() {
    yield takeEvery(authConstants.LOGIN_REQUEST, signIn);
    yield takeEvery(authConstants.LOGOUT_REQUEST, logOut);

    yield takeEvery(todoConstants.GET_TODO_LIST_REQUEST, getTodoList);
    yield takeEvery(todoConstants.GET_TODO_REQUEST, getTodo);
}
