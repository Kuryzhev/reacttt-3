import { put } from "redux-saga/effects";

import { authActions } from "../redux/actions/authActions";
import { setLoginInStorage, clearStorage } from "../helpers/sessionStorageHelpers";

export function* signIn(action) {
    const {username} = action.payload;
    try {
        // let loginResult = yield call(() => authApi.signIn(username));
        setLoginInStorage(username);
        yield put(authActions.login_success(username));
    } catch (e) {
        yield put(authActions.login_failure(e));
    }
}

export function* logOut() {
    try {
        // authApi.logOut();
        clearStorage();
        yield put(authActions.logout_success());
    } catch (e) {
        yield put(authActions.logout_failure());
    }
}
