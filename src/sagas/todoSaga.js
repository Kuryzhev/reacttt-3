import { call, put } from "redux-saga/effects";

import { todoActions } from "../redux/actions/todoActions";
import { todoApi } from "../api/todoApi";

export function* getTodoList() {
    try {
        let response = yield call(() => todoApi.getTodoList());
        yield put(todoActions.get_todoList_success(response));
    } catch (e) {
        yield put(todoActions.get_todoList_failure(e));
    }
}

export function* getTodo(action) {
    try {
        let response = yield call(() => todoApi.getTodo(action.payload));
        yield put(todoActions.get_todo_success(response))
    } catch (e) {
        yield put(todoActions.get_todo_failure(e))
    }

}
