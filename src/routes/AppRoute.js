import React from "react";
import { Router, Switch, Redirect } from "react-router-dom";

import { history } from "../helpers/history";
import { PublicRoute } from "./PublicRoute";
import { PrivateRoute } from "./PrivateRoute";
import { Header } from "../components/Header";
import { Login } from "../pages/Login";
import { TodoList } from "../pages/TodoList";
import { Todo } from "../pages/Todo";

export const AppRoute = () => {
    return (
        <Router history={history}>
            <div>
                <Header/>
                <Switch>
                    <PublicRoute path="/login" component={Login}/>
                    <PrivateRoute path="/todos" component={TodoList}/>
                    <PrivateRoute path="/todo/:id" component={Todo}/>
                    <Redirect from='/' to='/todos'/>
                </Switch>
            </div>
        </Router>
    );
};
