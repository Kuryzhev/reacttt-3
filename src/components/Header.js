import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { authActions } from "../redux/actions/authActions";
import "./Header.css";

export const Header = () => {
    const dispatch = useDispatch();
    const isAuthorized = useSelector(state => state.auth.isAuthenticated);

    const logout = () => {
        dispatch(authActions.logout_request());
    };

    const authorized = () => {
        return (
            <>
                <Link to="/todos" className="link">Todo List</Link>
                <button onClick={logout} className="btn">Log out</button>
            </>

        );
    };

    return (
        <header>
            <div className="tool-bar">
                {isAuthorized && authorized()}
            </div>
        </header>
    );
};

