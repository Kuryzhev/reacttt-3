export const getUser = JSON.parse(sessionStorage.getItem("user"));
export const clearStorage = () => sessionStorage.removeItem("user");
export const setLoginInStorage = (username) => sessionStorage.setItem("user", JSON.stringify(username));
