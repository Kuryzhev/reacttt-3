import React, { useState } from "react";
import { useDispatch } from "react-redux";

import { authActions } from "../redux/actions/authActions";
import "../App.css";

export const Login = () => {
    const dispatch = useDispatch();
    const [user, setUser] = useState({
        username: '',
        password: ''
    });

    const handleChange = (e) => {
        const {name, value} = e.target;
        setUser(user => ({...user, [name]: value}))
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (user.username) {
            dispatch(authActions.login_request(user));
        }
    };

    return (
        <div className="container">
            <h2 className="title">Login</h2>
            <form name="form" onSubmit={handleSubmit}>
                <div className="login-section">
                    <label>Username:</label>
                    <input type="text"
                           name="username"
                           onChange={handleChange}
                    />
                </div>
                <button className="btn" type="submit">Sign In</button>
            </form>
        </div>
    )
};
