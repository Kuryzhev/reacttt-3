import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { todoActions } from "../redux/actions/todoActions";
import { history } from "../helpers/history";
import "../App.css";

export const TodoList = () => {
    const dispatch = useDispatch();
    const todoList = useSelector(state => state.todo.todoList);

    useEffect(() => {
        dispatch(todoActions.get_todoList_request())
    }, [dispatch]);

    const todo = () => {
        return todoList.map(todo => {
            return (
                <div key={todo.id} className="item">
                    <p>Title: {todo.title}</p>
                    <button className="item-bnt"
                            onClick={() => history.push(`/todo/${todo.id}`)}
                    >
                        Details
                    </button>
                </div>
            )
        })
    };

    return (
        <main>
            <div className="container">
                <h1 className="title">Todo List</h1>
                {todoList && todo()}
            </div>
        </main>
    )
};
