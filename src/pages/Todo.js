import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { todoActions } from "../redux/actions/todoActions";
import "../App.css";

export const Todo = () => {
    const dispatch = useDispatch();
    let {id} = useParams();

    const todoDetails = useSelector(state => state.todo.todoDetails);
    const todoError = useSelector(state => state.todo.todoError);

    useEffect(() => {
        dispatch(todoActions.get_todo_request(id));
        return () => {
            dispatch(todoActions.remove_todo())
        }
    }, [id, dispatch]);

    return (
        <div className="container">
            {todoError && <p>{todoError}</p>}
            {todoDetails && (
                <div>
                    <p>Title: {todoDetails.title}</p>
                    <p>User ID: {todoDetails.userId}</p>
                    <p>ID: {todoDetails.id}</p>
                    <p className={todoDetails.completed ? "completed" : "not-completed"}>
                        Status: {todoDetails.completed ? "Completed" : "Not completed"}
                    </p>
                </div>
            )}
        </div>
    )
};
