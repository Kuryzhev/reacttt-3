import { api } from "./configApi";

export const todoApi = {
    getTodoList,
    getTodo
};

function getTodoList() {
    return api
        .get("/todos")
        .then(res => res.data)
        .catch(err => {
            throw err
        });
}

function getTodo(id) {
    return api
        .get(`/todos/${id}`)
        .then(res => res.data)
        .catch(err => {
            throw err
        });
}
