import axios from "axios";
import { environment } from "../enviroment";

export const api = axios.create({
    baseURL: `${environment}`,
    headers: {
        "Content-Type": "application/json",
    },
});
